package main

import (
	"flickrgopher/common"
	"github.com/codegangsta/martini"
	"github.com/codegangsta/martini-contrib/render"
)

func DB() martini.Handler {
	return func(c martini.Context) {
		data := common.NewData("photos")
		c.Map(data)
		defer data.Close()
		c.Next()
	}
}

func main() {
	m := martini.Classic()
	m.Use(render.Renderer())
	m.Use(DB())

	m.Get("/", func(r render.Render, data *common.Data) {
		r.HTML(200, "index", data.GetAlbums())
	})

	m.Run()
}
