package common

import (
	"fmt"
	"labix.org/v2/mgo"
	"labix.org/v2/mgo/bson"
	"time"
)

var gs *mgo.Session

func init() {
	s, err := mgo.Dial("mongodb://localhost")
	if err != nil {
		panic(err)
	}
	gs = s
}

type AlbumFile struct {
	RemoteName     string    `bson:"remoteName"`
	LocalPath      string    `bson:"localPath"`
	LocalTimeStamp time.Time `bson:"localTimeStamp"`
	UploadDate     time.Time `bson:"uploadDate"`
}

type AlbumError struct {
	Message        string    `bson:"message"`
	LocalPath      string    `bson:"localPath"`
	LocalTimeStamp time.Time `bson:"localTimeStamp"`
	ErrorDate      time.Time `bson:"errorDate"`
}

type Album struct {
	Id     bson.ObjectId `bson:"_id"`
	Name   string        `bson:"name"`
	Files  []AlbumFile   `bson:"files"`
	Errors []AlbumError  `bson:"errors"`
}

type Data struct {
	s  *mgo.Session
	db *mgo.Database
}

func NewAlbum(name string) *Album {
	return &Album{"", name, []AlbumFile{}, []AlbumError{}}
}

func NewAlbumFile(localPath string, lastModified time.Time) *AlbumFile {
	return &AlbumFile{"", localPath, lastModified, time.Now()}
}

func NewAlbumError(localPath string, message string) *AlbumError {
	return &AlbumError{message, localPath, time.Now(), time.Now()}
}

func NewData(dbname string) *Data {
	sess := gs.Clone()
	return &Data{sess, sess.DB(dbname)}
}

func (a *Album) HasErrors() bool {
	return len(a.Errors) > 0
}

func (d *Data) GetAlbums() []Album {
	var albums []Album
	d.db.C("albums").Find(nil).All(&albums)
	return albums
}

func (d *Data) GetAlbum(name string) (*Album, bool) {
	album := Album{}
	err := d.db.C("albums").Find(bson.M{"name": name}).One(&album)
	if err != nil {
		return nil, false
	}
	return &album, true
}

func (d *Data) GetOrCreateAlbum(name string) *Album {
	a, ok := d.GetAlbum(name)
	if !ok {
		fmt.Println("Created album ", name)
		a = NewAlbum(name)
		a.Id = bson.NewObjectId()
		d.db.C("albums").Insert(a)
	}

	return a
}

func (d *Data) RemoveFileFromAlbum(id bson.ObjectId, localPath string) {
	d.db.C("albums").Update(bson.M{"_id": id}, bson.M{"$pull": bson.M{"files": bson.M{"localPath": localPath}}})
	d.db.C("albums").Update(bson.M{"_id": id}, bson.M{"$pull": bson.M{"errors": bson.M{"localPath": localPath}}})
}

func (d *Data) AddFileToAlbum(id bson.ObjectId, localPath string, lastUpdate time.Time) {
	af := NewAlbumFile(localPath, lastUpdate)
	d.db.C("albums").Update(bson.M{"_id": id}, bson.M{"$push": bson.M{"files": af}})
}

func (d *Data) Close() {
	d.s.Close()
}
