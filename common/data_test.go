package common

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestGetAlbums(t *testing.T) {
	db := getTestDatabase()

	albums := db.GetAlbums()
	assert.Equal(t, 0, len(albums))

	_, ok := db.GetAlbum("asdf")
	assert.False(t, ok)

	db.Close()
}

func TestGetOrCreateAlbum(t *testing.T) {
	db := getTestDatabase()

	a := db.GetOrCreateAlbum("test1")
	b := db.GetOrCreateAlbum("test1")
	assert.Equal(t, a.Id, b.Id)
}

func TestAddRemoveFile(t *testing.T) {
	db := getTestDatabase()

	a := db.GetOrCreateAlbum("test1")

	db.AddFileToAlbum(a.Id, "a/b/c", time.Now())
	db.AddFileToAlbum(a.Id, "b/c/d", time.Now())

	b := db.GetOrCreateAlbum("test1")
	assert.Equal(t, 2, len(b.Files))
	assert.Equal(t, "a/b/c", b.Files[0].LocalPath)

	db.RemoveFileFromAlbum(a.Id, "a/b/c")

	c := db.GetOrCreateAlbum("test1")
	assert.Equal(t, 1, len(c.Files))
	assert.Equal(t, "b/c/d", c.Files[0].LocalPath)
}

func getTestDatabase() *Data {
	db := NewData("photos_test")
	db.db.C("albums").RemoveAll(nil)
	db.db.C("photos").RemoveAll(nil)
	return db
}
