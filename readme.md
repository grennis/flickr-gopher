
FlickrGopher
============

Scans a local directory tree on your PC and uploads images 
and videos into corresponding albums on Flickr.

Incrementally adds and updates files as you add and update them
on your local drive.

Includes worker process (processor.go) that handles that sync and uploading, and
web interface (web.go) that shows albums and files and sync status with 
the online versions.

Uses Martini for web interface and go-flickr for the Flickr API access.

