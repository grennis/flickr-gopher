package main

import (
	"flickrgopher/common"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

var f *Filter

func init() {
	f = NewFilter(nil, nil, nil)
}

func TestRemove(t *testing.T) {
	assert.Equal(t, []string{"a", "b", "d"}, f.removeAtIndex([]string{"a", "b", "c", "d"}, 2))
	assert.Equal(t, []string{"a", "b", "c"}, f.removeAtIndex([]string{"a", "b", "c", "d"}, 3))
}

func TestInAlbum(t *testing.T) {
	album := createTestAlbum("test", []string{"/a/x", "/a/y", "/a/z"})
	assert.False(t, f.fileInAlbum("/a/b", album))
	assert.True(t, f.fileInAlbum("/a/y", album))
}

func TestProcessAlbumNotFound(t *testing.T) {
	f := makeFilter()

	item := NewScanItem("test2", []string{"/a/x"})
	go process(f, item, nil)

	msgs := drainMessages(f.out)
	assert.Equal(t, 1, len(msgs))
}

func TestProcessAlbumSame(t *testing.T) {
	f := makeFilter()
	album := createTestAlbum("test1", []string{"/a/x", "/a/y", "/a/z"})

	item := NewScanItem("test1", []string{"/a/x", "/a/y", "/a/z"})
	go process(f, item, album)

	msgs := drainMessages(f.out)
	assert.Equal(t, 0, len(msgs))
}

func TestProcessAlbumPartial(t *testing.T) {
	f := makeFilter()
	album := createTestAlbum("test1", []string{"/a/x", "/a/y", "/a/z"})

	item := NewScanItem("test2", []string{"/a/x", "/a/q", "/a/z"})
	go process(f, item, album)

	msgs := drainMessages(f.out)
	assert.Equal(t, 1, len(msgs))
	assert.Equal(t, 1, len(msgs[0].Files))
	assert.Equal(t, "/a/q", msgs[0].Files[0])
}

func makeFilter() *Filter {
	in := make(chan ScanItem)
	out := make(chan ScanItem)
	return NewFilter(nil, in, out)
}

func process(f *Filter, item *ScanItem, album *common.Album) {
	f.processAlbum(item, album)
	close(f.out)
}

func drainMessages(ch chan ScanItem) []ScanItem {
	items := make([]ScanItem, 0)

	for item := range ch {
		items = append(items, item)
	}

	return items
}

func createTestAlbum(name string, files []string) *common.Album {
	a := common.NewAlbum(name)

	for _, f := range files {
		af := common.NewAlbumFile(f, time.Now())
		a.Files = append(a.Files, *af)
	}

	return a
}
