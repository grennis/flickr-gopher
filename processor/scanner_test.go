package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

var s *Scanner

func init() {
	s = NewScanner(nil, "/")
}

func TestIsYear(t *testing.T) {
	assert.Equal(t, false, s.isYear(""))
	assert.Equal(t, false, s.isYear("abcd"))
	assert.Equal(t, false, s.isYear("1111"))
	assert.Equal(t, true, s.isYear("2010"))
	assert.Equal(t, true, s.isYear("2020"))
}

func TestAlbumNames(t *testing.T) {
	_, ok := s.albumName("")
	assert.Equal(t, false, ok)

	_, ok = s.albumName("asdfasdf")
	assert.Equal(t, false, ok)

	_, ok = s.albumName("20140101")
	assert.Equal(t, false, ok)

	_, ok = s.albumName("2014-01-01")
	assert.Equal(t, false, ok)

	name, ok := s.albumName("2014-01-01 test this one")
	assert.Equal(t, true, ok)
	assert.Equal(t, name, "test this one")
}

func TestExtensions(t *testing.T) {
	assert.True(t, s.isMediaFile("test.jpg"))
	assert.False(t, s.isMediaFile("test.foo"))
}
