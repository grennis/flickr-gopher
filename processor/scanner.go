package main

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
)

var extensions map[string]bool

func init() {
	extensions = map[string]bool{
		".jpg":  true,
		".jpeg": true,
		".mp4":  true,
		".mov":  true,
		".avi":  true,
	}
}

// Scans the filesystem and sends the files to be uploaded to the given channel
type Scanner struct {
	ch       chan ScanItem
	rootPath string
}

// Represents a single photo or video to be uploaded
type ScanItem struct {
	Album string
	Files []string
}

func NewScanner(ch chan ScanItem, rootPath string) *Scanner {
	return &Scanner{ch, rootPath}
}

func NewScanItem(album string, files []string) *ScanItem {
	return &ScanItem{album, files}
}

func (s *Scanner) scan() {
	files, err := ioutil.ReadDir(s.rootPath)
	if err != nil {
		panic(err)
	}

	for _, f := range files {
		if f.IsDir() && s.isYear(f.Name()) {
			s.processYearDir(s.rootPath + "/" + f.Name())
		}
	}

	close(s.ch)
}

func (s *Scanner) processYearDir(path string) {
	albums, err := ioutil.ReadDir(path)
	if err != nil {
		panic(err)
	}

	for _, f := range albums {
		if f.IsDir() {
			an, ok := s.albumName(f.Name())

			if ok {
				s.processAlbumDir(path+"/"+f.Name(), an)
			}
		}
	}
}

func (s *Scanner) processAlbumDir(path string, an string) {
	fmt.Println("Scanning", an, "from", path)

	files, err := ioutil.ReadDir(path)
	if err != nil {
		panic(err)
	}

	mediaFiles := make([]string, 0, len(files))

	for _, f := range files {
		if !f.IsDir() && s.isMediaFile(f.Name()) {
			mediaFiles = append(mediaFiles, path+"/"+f.Name())
		}
	}

	if len(mediaFiles) > 0 {
		si := NewScanItem(an, mediaFiles)
		s.ch <- *si
	}
}

func (*Scanner) isYear(name string) bool {
	v, err := strconv.ParseInt(name, 0, 16)

	return err == nil && v > 1990 && v < 2200
}

func (*Scanner) albumName(name string) (string, bool) {
	r := regexp.MustCompile(`\d\d\d\d-\d\d-\d\d\s(.*)`)
	m := r.FindStringSubmatch(name)

	if len(m) > 1 {
		return m[1], true
	}

	return "", false
}

func (*Scanner) isMediaFile(name string) bool {
	ext := filepath.Ext(name)

	return extensions[strings.ToLower(ext)]
}
