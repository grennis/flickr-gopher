package main

import (
	"flickrgopher/common"
)

// Filters the scanned items and removes the items that have already been uploaded
type Filter struct {
	db  *common.Data
	in  chan ScanItem
	out chan ScanItem
}

func NewFilter(db *common.Data, in chan ScanItem, out chan ScanItem) *Filter {
	return &Filter{db, in, out}
}

func (f *Filter) filter() {
	for i := range f.in {
		a, _ := f.db.GetAlbum(i.Album)
		f.processAlbum(&i, a)
	}

	close(f.out)
}

func (f *Filter) processAlbum(item *ScanItem, album *common.Album) {
	if album == nil {
		// Entire album is not in database - upload the whole thing
		f.out <- *item
		return
	}

	for j := len(item.Files) - 1; j >= 0; j-- {
		if f.fileInAlbum(item.Files[j], album) {
			item.Files = f.removeAtIndex(item.Files, j)
		}
	}

	if len(item.Files) > 0 {
		f.out <- *item
	}
}

func (f *Filter) fileInAlbum(path string, album *common.Album) bool {
	for _, f := range album.Files {
		if f.LocalPath == path {
			return true
		}
	}
	return false
}

func (f *Filter) removeAtIndex(list []string, i int) []string {
	return append(list[:i], list[i+1:]...)
}
