package main

import (
	"flickrgopher/common"
	"fmt"
	"github.com/mncaudill/go-flickr"
	"os"
)

// Accepts the filtered items and uploads to flickr
type Uploader struct {
	db *common.Data
	in chan ScanItem
}

func NewUploader(db *common.Data, in chan ScanItem) *Uploader {
	return &Uploader{db, in}
}

func (u *Uploader) upload() (int, int, int) {
	fcc, ecc, acc := 0, 0, 0

	for i := range u.in {
		fc, ec := u.uploadItem(i)

		if fc > 0 || ec > 0 {
			acc++
			fcc += fc
			ecc += ec
		}
	}

	return acc, fcc, ecc
}

func (u *Uploader) uploadItem(item ScanItem) (int, int) {
	fc, ec := 0, 0
	a := u.db.GetOrCreateAlbum(item.Album)

	for _, f := range item.Files {
		u.db.RemoveFileFromAlbum(a.Id, f)

		args := map[string]string{}
		r := &flickr.Request{"2df9e6156fd4af3ae5f36b4c55e30af8", "", args}
		r.Sign("3b2864c3d8ace339")
		resp, err := r.Upload(f, "image/jpg")
		if err != nil {
			panic(err)
		}

		fmt.Println("Status " + resp.Status)

		if resp.Error != nil {
			fmt.Println("Error code " + resp.Error.Code)
			fmt.Println("Error msg " + resp.Error.Message)
		}

		fmt.Println(resp.Payload)

		fmt.Println("Uploaded", f)
		fi, err := os.Stat(f)
		if err != nil {
			panic(err)
		}

		u.db.AddFileToAlbum(a.Id, f, fi.ModTime())
		fc++
	}

	return fc, ec
}
