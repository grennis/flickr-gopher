package main

import (
	"flag"
	"flickrgopher/common"
	"fmt"
	"os/user"
	"strings"
)

var rootPath = flag.String("dir", "~", "Root directory to start scan")
var database = flag.String("db", "photos", "Mongo database name")

func main() {
	flag.Parse()

	db := common.NewData(*database)
	defer db.Close()

	all := make(chan ScanItem)
	filtered := make(chan ScanItem)

	usr, err := user.Current()
	if err != nil {
		panic(err)
	}
	*rootPath = strings.Replace(*rootPath, "~", usr.HomeDir, -1)

	s := NewScanner(all, *rootPath)
	go s.scan()

	f := NewFilter(db, all, filtered)
	go f.filter()

	u := NewUploader(db, filtered)
	ac, fc, ec := u.upload()

	fmt.Println("Finishined uploading files", fc, "into", ac, "albums with", ec, "errors.")
}
